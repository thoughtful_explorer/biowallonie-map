# Biowallonie Store Extractor
Extracts Biowallonie (French-speaking Belgian Walloon region organic consotrium) shop locations (points de vent) from the official website's API, geocodes them, and then places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for calling the API
    * JSON for parsing the API response
    * Geocoder for retrieving geographic coordinates for addresses from OpenStreetMap's Nominatim service 
    * Simplekml for easily building KML files
* Also of course depends on the [Algolia](https://www.algolia.com/) API used by the official [Biowallonie](https://www.biowallonie.com/acteursbio/) webpage.
