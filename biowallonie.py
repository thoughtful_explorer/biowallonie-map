#!/usr/bin/python3

import requests
import json
import geocoder
import simplekml

#Set filename/path for KML file output
kmlfile = "biowallonie.kml"
#Set KML schema name
kmlschemaname = "biowallonie"
#Set API URL
apiURL = "https://1z5scxypgz-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia for vanilla JavaScript (lite) 3.24.5;instantsearch.js 2.2.5;JS Helper 2.23.0&x-algolia-application-id=1Z5SCXYPGZ&x-algolia-api-key=97a4f110fedadb45ebea3a8d7149dee4"
#Surround payload with three double quotes in order for Python to keep all the double quotes intact for retaining valid JSON, and modify the query to 400 hitsPerPage to get all store data in one call
payload = """{"requests":[{"indexName":"wp_posts_post","params":"query=&hitsPerPage=400&facets=%5B%22taxonomies.acteurs%22%2C%22taxonomies.produits%22%2C%22taxonomies.villes%22%2C%22taxonomies.cp%22%2C%22taxonomies.province%22%5D&tagFilters=&facetFilters=%5B%5B%22taxonomies.acteurs%3APoint%20de%20vente%22%5D%5D"}]}"""
#Get the API response and JSONify it
response = requests.post(apiURL,data=payload)
response = response.json()

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through JSON for each store
for hit in response["results"][0]["hits"]:
    #Get the name of the store and add Bio shop prefix to reduce vagueness if presented among other map layers
    storename = "Bio shop - " + hit["post_title"]
    #Get and form the store address, separating address and city with a comma and a space
    storeaddress = hit["acf_articles_address"] +", "+ hit["taxonomies"]["ville"][0]
    #Run the geocode
    geo = geocoder.osm(storeaddress)
    #Skip over stores with failed geocodes - it would be nice to fix the geocodes without results, but 68 out of 360 is decent enough for now 
    if geo.ok == False:
        continue
    #Get the coordinates from the geocode
    lat = geo.y
    lng = geo.x
    #First, create the point name and description in the kml
    point = kml.newpoint(name=storename,description="market")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(lng, lat)]
#Save the final KML file
kml.save(kmlfile)
